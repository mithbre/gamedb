import dbg

def game_insert(db):
	name = raw_input("Name: ")
	db.show_systems()
	print(" 0: Not listed")
	systemID = input("System: ")
	if not systemID:
		system = raw_input("System: ")
		systemID = db.insert_system(system)
		if dbg.DEBUG:
			print("DEBUG: insert.game_insert  systemID: %s" % (systemID))

	own = input("own?(0/1): ")
	finished = input("finished?(0/1): ")
	rom = input("rom?(0/1): ")
	
	gameID = db.insert_game(name, own, finished, rom, systemID)
	if dbg.DEBUG:
		print("DEBUG: insert.game_insert  gameID: %s" % (gameID))
	db.insert_platform(gameID, systemID)
	

def insert_mode(db):
	while True:
		print("\nInsert")
		print("------")
		print(" 1: New game (partially implemented)")
		print(" 2: New system (partially implemented)")
		print(" 0: Menu")
		mode = input("choice: ")
		if mode is 1:
			game_insert(db)
		elif mode is 2:
			db.insert_system(raw_input("System: "))
		elif mode is 0:
			return
		else:
			print("Try again")