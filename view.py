def game_select(db):
	print("\nView games")
	print("----------")
	print(" 1: All")
	print(" 2: By System")
	print(" 3: By own")
	print(" 4: By finished")
	print(" 5: By rom")
	print(" 0: Menu")
	foo = input("List games by: ")
	
	if foo == 1:
		pass
		#all
	elif foo == 2:
		db.show_systems()
		systemID = input("System: ")
		games = db.select_game_by_system(systemID)
		for g in games:
			print("%s|%s|%s|%s" % (g[0], g[1], g[2], g[3]))
	elif foo == 3:
		print("\n 1: Show owned games")
		print(" 0: Show wanted games")
		owned = input("choice: ")
		games = db.select_game_by_ownership(owned)
		for g in games:
			print("%30s\t%s" % (g[0], g[1]))
	elif foo == 4:
		print("\n 1: Show finished games")
		print(" 0: Show unfinished games")
		finished = input("choice: ")
		games = db.select_game_by_finished(finished)
		for g in games:
			print("%30s\t%s" % (g[0], g[1]))
	elif foo == 5:
		print("\n 1: Show ROM games")
		print(" 0: Show non-ROM games")
		rom = input("choice: ")
		games = db.select_game_by_rom(rom)
		for g in games:
			print("%30s\t%s" % (g[0], g[1]))
	elif foo == 0:
		return

	raw_input("Press any key to continue...")
	
	

def view_mode(db):
	while True:
		print("\nView Mode")
		print("---------")
		print(" 1: Games")
		print(" 2: Systems")
		print(" 0: Menu")
		mode = input("choice: ")
	
		if mode == 1:
			game_select(db)
		elif mode == 2:
			db.show_systems()
		elif mode == 0:
			return
		else:
			pass