import sqlite3, dbg

#Problem with Schema
#TODO: Allow for the same game on separate systems to have different attributes.

class Database(object):
	def __init__(self, file):
		print("Database: Not fully implemented")
		self.con = sqlite3.connect(file)
		self.cur = self.con.cursor()

	def create(self):
		self.cur.execute("CREATE TABLE System ( \
				system_id	INTEGER		NOT NULL PRIMARY KEY AUTOINCREMENT, \
				name		TEXT		NOT NULL UNIQUE)")
		self.cur.execute("CREATE TABLE Game ( \
				game_id		INTEGER		NOT NULL PRIMARY KEY AUTOINCREMENT, \
				name		TEXT		NOT NULL, \
				own		INTEGER		DEFAULT NULL, \
				finished	INTEGER		DEFAULT NULL, \
				rom		INTEGER		DEFAULT NULL)")
		self.cur.execute("CREATE TABLE Platform ( \
				game_id		INTEGER		NOT NULL REFERENCES Game (game_id), \
				system_id	INTEGER		NOT NULL REFERENCES System (system_id))")
		self.con.commit()


	def select_system(self, system):
		if dpg.DEBUG:
			print("DEBUG: database.select_system \"%s\"" % (system))
		try:
			self.cur.execute("SELECT system_id FROM System WHERE name = ?", [system])
			sysId = self.cur.fetchone()
			return sysId[0]
		except:
			print("Congrats, you found an bug.")


	def insert_system(self, system):
		if dbg.DEBUG:
			print("DEBUG: database.insert_system \"%s\"" % (system))
		try:
			self.cur.execute("INSERT INTO System(name) VALUES (?)", [system])
			self.con.commit()
			return self.cur.lastrowid
		except sqlite3.IntegrityError:
			print("Already in Database...skipping")
			#TODO: retrieve rowid
			#TODO: check for wEIrdNEsS


	def insert_platform(self, game, system):
		if dbg.DEBUG:
			print("DEBUG: database.insert_platform -- game: %s, sys: %s" % (game, system))
		self.cur.execute("INSERT INTO Platform VALUES (?, ?)", (game, system))
		self.con.commit()


	def select_game_by_system(self, system):
		self.cur.execute("SELECT name, own, finished, rom \
				FROM Game \
				INNER JOIN Platform \
				ON Game.game_id = Platform.game_id \
				WHERE Platform.system_id = ? \
				ORDER BY Game.name", [system])
		return self.cur.fetchall()


	def select_game_by_ownership(self, owned):
		self.cur.execute("SELECT Game.name, System.name \
				FROM Game, System \
				INNER JOIN Platform \
				ON Game.game_id = Platform.game_id \
					AND System.system_id = Platform.system_id \
				WHERE Game.own = ? \
				ORDER BY System.name, Game.name", [owned])
		return self.cur.fetchall()


	def select_game_by_finished(self, finished):
		self.cur.execute("SELECT Game.name, System.name \
				FROM Game, System \
				INNER JOIN Platform \
				ON Game.game_id = Platform.game_id \
					AND System.system_id = Platform.system_id \
				WHERE Game.finished = ? \
				ORDER BY System.name, Game.name", [finished])
		return self.cur.fetchall()


	def select_game_by_rom(self, rom):
		self.cur.execute("SELECT Game.name, System.name \
				FROM Game, System \
				INNER JOIN Platform \
				ON Game.game_id = Platform.game_id \
					AND System.system_id = Platform.system_id \
				WHERE Game.rom = ? \
				ORDER BY System.name, Game.name", [rom])
		return self.cur.fetchall()


	def insert_game(self, name, own, finished, rom, system):
		if dbg.DEBUG:
			print("DEBUG: database.insert_game \"%s\"" % (name))
		self.cur.execute("INSERT INTO Game(name, own, finished, rom) \
				VALUES (?, ?, ?, ?)", (name, own, finished, rom))
		self.con.commit()
		return self.cur.lastrowid


	def show_systems(self):
		if dbg.DEBUG:
			print("\nDatabase: show_sytems")
		self.cur.execute("SELECT * FROM System")
		systems = self.cur.fetchall()
		for i in systems:
			print("%2d: %s" % (i[0], i[1]) )

