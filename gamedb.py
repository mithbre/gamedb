import sys, os, database, view, insert, edit, delete, dbg
DEBUG = 0

def _mode():
	print("\nMenu")
	print("----")
	print(" 1: View   (partially implemented)")
	print(" 2: Insert (partially implemented)")
	print(" 3: Edit   (not implemented)")
	print(" 4: Delete (not implemented)")
	print(" 0: Exit")
	return input("Choice: ")

def main(argv):
	print("GameDB.py: Not fully implemented")
	if os.path.isfile(argv):
		if dbg.DEBUG:
			print("GameDB.py: DB exists")
		db = database.Database(argv)
	else:
		if DEBUG:
			print("GameDB.py: DB does not exist...Creating.")
		db = database.Database(argv)
		db.create()
		if DEBUG:
			print("GameDB.py: DB created.")

	while True:
		mode = _mode()
		if mode is 0:
			sys.exit(0)
		elif mode is 1:
			view.view_mode(db)
		elif mode is 2:
			insert.insert_mode(db)
		elif mode is 3:
			edit.edit_mode(db)
		elif mode is 4:
			delete.delete_mode(db)
		else:
			print("Try again\n")


if __name__ == "__main__":
	if len(sys.argv) > 1:
		main(sys.argv[1])
	else:
		print("Usage: $ gamedb.py [database]")
